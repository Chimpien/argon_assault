﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour {
    //script to auto-destroy run-time instantiated objects after a fixed time
    //used for the spapwned deathFX objects
	// Use this for initialization
	void Start ()
    {
        //Destroy(gameObject, gameObject.GetComponent<ParticleSystem>().duration);//destroy the explosion object after it has finished
        Destroy(gameObject, 4f);//have used a fixed time as the sound lasts longer than the explosion
		
	}
	
	
}

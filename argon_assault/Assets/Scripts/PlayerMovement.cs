﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMovement : MonoBehaviour {

    [Header("Movement")]
    [Tooltip("in ms^-1")][SerializeField] float speed = 10f;
    [SerializeField] float xRange = 4f;
    [SerializeField] float yRange = 2f;

    [Header("Rotations")]
    [SerializeField] float pitchFactor = 5f;
    [SerializeField] float throwPitchFactor = 30f;
    [SerializeField] float yawFactor = 5f;
    [SerializeField] float throwRollFactor = 30f;

    [Header("Object References")]
    [SerializeField] ParticleSystem[] lasers;//expose a location for an array of ParticleSystems. They can then be dragged into the array slots if the array size was set, or dragged in as a group and this auto sets the array size

    Dictionary<string, float> throws = new Dictionary<string, float>();
    private bool ControlIsEnabled = true;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (ControlIsEnabled)
        {
            processTranslation();
            processRotation();
            processFiring();
        }
    }

    private void processFiring()
    {
        if(CrossPlatformInputManager.GetButton("Fire1"))//in order for this to work "Fire1" must be defined in Edit > Project Settings > Input
        {
            SetGunState(true);//set both guns to on when "Fire1" is pressed            
        }
        else
        {
            SetGunState(false);
        }
    }

    private void SetGunState(bool active)
    {
        foreach (ParticleSystem laser in lasers)
        {
            var emitter = laser.emission;// the emission module has to be stored in a temp variable to allow setting of the enable parameter
            emitter.enabled = active;// as it has no setter method. Not quite sure why this gets round it!
        }
    }

    private void processRotation()//modify ship rotation to look nicer during movement
    {
        
        float pitch = (transform.localPosition.y * -pitchFactor) + (throws["Vertical"] * -throwPitchFactor);
        float yaw = transform.localPosition.x * yawFactor;
        float roll = throws["Horizontal"] * -throwRollFactor;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void processTranslation()//move the ship. Use input as a velocity setting for the associated direction
    {
        float xPosition = getInput("Horizontal", xRange, transform.localPosition.x);
        float yPosition = getInput("Vertical", yRange, transform.localPosition.y);

        transform.localPosition = new Vector3(xPosition, yPosition, transform.localPosition.z);
    }

    private float getInput(string axis, float range, float localPosition)//get an input axis value and convert it to an updated position
    {     
        throws[axis] = CrossPlatformInputManager.GetAxis(axis);//get the axis
        float offsetFrame = throws[axis] * speed * Time.deltaTime;//get the change in position due to the velocity
        float rawPosition = localPosition + offsetFrame;//calculate the absolute position
        float Position = Mathf.Clamp(rawPosition, -range, range);//clamp the position to the specified value range
        return Position;
    }    

    private void OnPlayerDeath() //called by string handle from CollisionHandler
    {
        //print("Controls Disabled");
        ControlIsEnabled = false;//disable the player controls on death
    }
    
}

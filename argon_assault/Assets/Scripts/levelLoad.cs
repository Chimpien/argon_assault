﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class levelLoad : MonoBehaviour {

    private void Awake()
    {
        int numMusicPlayers = FindObjectsOfType<levelLoad>().Length;//can find objects of predefined types or scripts (by name)
        if(numMusicPlayers > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    // Use this for initialization
    void Start () {
        Invoke("SceneChange", 5);

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void SceneChange()
    {
        SceneManager.LoadScene(1);
    }
}

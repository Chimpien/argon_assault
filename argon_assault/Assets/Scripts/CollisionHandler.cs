﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour {
    //class to handle player collisions
    [SerializeField] GameObject deathFX;
    [SerializeField] float lvlLoadDelay = 2f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        //print("triggered");
        PlayerDeath();
    }

    private void PlayerDeath()
    {
        SendMessage("OnPlayerDeath");//Call the named function in another script. Messages are only received by scripts on the same game object!
        deathFX.SetActive(true);//Play the death animation. Todo it would be better to use the same template as the enemy deaths so the ship disappears and spawns the deathFX
        Invoke("ReloadScene",lvlLoadDelay);//restart the lvl on death

    }

    private void ReloadScene()
    {
        SceneManager.LoadScene(1);
    }
}

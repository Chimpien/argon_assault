﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField] GameObject deathFX;
    [SerializeField] Transform parent;
    [SerializeField] int scorePerHit = 10;
    [SerializeField] int hits = 10;

    ScoreBoard scoreBoard;

	// Use this for initialization
	void Start ()
    {
        AddNonTriggerBoxCollider();//add box colliders at run time to remove dependence on component existing in prefab - important with imported assets as they may need to be reimported an overwrite local changes
        scoreBoard = FindObjectOfType<ScoreBoard>();//can find a ref to a script on another gameobject	
	}

    private void AddNonTriggerBoxCollider()
    {
        Collider boxCollider = gameObject.AddComponent<BoxCollider>();//add component in script
        boxCollider.isTrigger = false;
    }

    
    private void OnParticleCollision(GameObject other)
    {
        ProcessHits();
        if (hits <= 0)//when there are no more hits
        {
            die();
        }
    }

    private void ProcessHits()
    {//when hit decrease hit counter by one
        scoreBoard.ScorePerHit(scorePerHit);
        hits--;
        //todo add hit FX?
    }

    private void die()
    {//spawn the deathFX prefab (particles plus sound) where the ship was, then destroy the ship
        GameObject fX = Instantiate(deathFX, transform.position, Quaternion.identity); //create an instance of the referenced game object at location where this object is with no rotation
        fX.transform.parent = parent;
        Destroy(gameObject);//destroy this
    }

    
}
